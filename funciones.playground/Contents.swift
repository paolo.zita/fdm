//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// categoria de mensualidades //
var promedio = 0
var pension = 0
var descuento = 0

func calcularpension(categoria:String) {
    if categoria == "A"{
        pension = 550
    }
    if categoria == "B"{
        pension = 500
    }
    if categoria == "C"{
        pension = 460
    }
    if categoria == "D"{
        pension = 400
    }
    print(pension)
}
calcularpension(categoria:"A")
calcularpension(categoria:"B")
calcularpension(categoria:"D")

func calculardescuento(promedio:Float){
    if promedio > 14 && promedio <= 16 {
      descuento = 10
    }
    if promedio > 17 && promedio <= 17.99 {
        descuento = 12
    }
    if promedio > 18 && promedio <= 20 {
        descuento = 15
    }
    
    print(descuento)
    
}
calculardescuento(promedio:16)