//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/* cuadrado bordes */
for num in 1...10 {
    for l in 1...10 {
        if (num == 1 || l == 10 || l==1 || num == 10) {
            print("*", terminator:"")
        }else {
            print(" ", terminator:"")
        }
        
    }
    print("")
}

/* genere 5 numeros al azar y calcule la suma de los numeros*/
var sum = 0
for r in 1...5 {
   var li = Int(arc4random_uniform(99))
    print(li)
    sum = sum + li
}
print(sum)

/*genere 5 numeros al azar y calcule el promedio de dichos numeros*/
var prom = 0
for i in 1...5 {
    var j = Int(arc4random_uniform(99))
    print(j)
    prom += j
}
print("promedio de los 5 numeros es \(prom/5)")