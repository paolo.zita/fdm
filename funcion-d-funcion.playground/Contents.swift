//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//definir promedio y descuento de alumnos
func definirpension(rango: String) -> Double {
    var pension = 0.0
    
    if rango == "A" {
        pension = 550.0
    }
    if rango == "B" {
        pension = 500.0
    }
    if rango == "C" {
        pension = 460.0
        
    }
    if rango == "D" {
        pension = 400.0
    }
    return pension
}


func definirpromedio(promedio: Double) -> Double {
    var descuento = 0.0
    if promedio == 14 || promedio <= 15.99 {
        descuento = 0.10
    }
    if promedio == 16 || promedio >= 17.99 {
        descuento = 0.12
    }
    if promedio == 18 || promedio >= 20 {
        descuento = 0.15
    }
    return descuento
}


func pagototal(promediofinal: Double, letra: String) -> Double {
    
    var nuevaPension = definirpension(rango: letra)
    var descuentopro = definirpromedio(promedio: 16)
    var descuentofinal = nuevaPension * descuentopro
    var pagototal = definirpension(rango: letra) - descuentofinal
    
    return pagototal
}

pagototal(promediofinal: 18, letra: "A")
pagototal(promediofinal: 18, letra: "B")


//-declarar del 1 al 4 segun su estado
func estadocivil(intervalo: Int) -> String {
    var estado = ""
    switch intervalo {
    case 1:
        estado = "soltero"
        break
    case 2:
        estado = "casado"
        break
    case 3:
        estado = "viudo"
        break
    case 4:
        estado = "divorsiado"
        break
    default:
        estado = "el parametro no valido"
        
    }
    return estado
}
estadocivil(intervalo: 2)



