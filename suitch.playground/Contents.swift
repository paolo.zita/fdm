//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
var producto = 1
var precio = 0

switch producto {
case  0:
   precio = 25
case 1:
    precio = 15
case 2:
    precio = 10
default:
    precio = 12
}

print(precio)

/* numero en letras */
var numero = 0
var letras = ""

switch numero {
case  0:
    letras = "cero"
case 1:
    letras = "uno"
case 2:
    letras = "dos"
case 3:
    letras = "tres"
case 4:
    letras = "cuatro"
case 5:
    letras = "cinco"
case 6:
    letras = "seis"
case 7:
    letras = "siete"
case 8:
    letras = "ocho"
case 9:
    letras = "nueve"
case 10:
    letras = "dies"
default:
    letras = "esta fuera de rango"
}

print(letras)

/*for*/
for num in 1...10{
    print("numero \(num)")
}
var diassemana = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"]
for d in diassemana {
    print(d)
}

/* diccionario for*/
let numberofleg = ["spider": 0, "ant": 6, "cat": 4]
for (animalname, legcount) in numberofleg{
    print("\(animalname)s have \(legcount) legs")
}


