//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//crea un arreglo con el dia de las semanas
var dias = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"]
print(dias)

// numeros aletorio de 3 digitos
var result:[Int] = []
for i in 0..<10 {
    result.append(Int(arc4random_uniform(999) + 100))
}
print (result)

//genere un arreglo con 10 numeros al azar, imprimir solo los numeros pares
var numAzar2:[Int] = []
for i in 0..<10 {
    var numero = Int(arc4random_uniform(99) + 10)
    if numero % 2 == 0 {
        print(numero)
    }
    numAzar2.append(numero)
}

//genere un arreglo con 10 numeros al azar
//imprimir el arreglo
//imprimir la suma de los items del arreglo
//imprimir el promedio de la suma de los items del arreglo
var result3:[Int] = []
for i in 0..<10 {
    result3.append(Int(arc4random_uniform(98) + 10))
}
print(result3)
var suma = result3.reduce(0, +)
print(suma)
var promedio = suma/2
print(promedio)

//genere un arreglo a con 10 numeros al azar 
//genere un arreglo b con 10 numeros al azar
//imprimir a 
//imprimir b
//sume el primero elemento de a con el primer elemento de b y asi sucesivamente con los 10 elementos y almacene el resultado en un arreglo c
//imprimir arreglo c

var resultA:[Int] = []
for i in 0..<10 {
    resultA.append(Int(arc4random_uniform(98) + 1))
}
var resultB:[Int] = []
for i in 0..<10 {
    resultB.append(Int(arc4random_uniform(98) + 1))
}

var c = [resultA[0]+resultB[0],resultA[1]+resultB[1],resultA[2]+resultB[2],resultA[3]+resultB[3],resultA[4]+resultB[4],resultA[5]+resultB[5],resultA[6]+resultB[6],resultA[7]+resultB[7],resultA[8]+resultB[8],resultA[9]+resultB[9]]
print ("numeros al azar de a \(resultA)")
print ("numeros al azar de b \(resultB)")


print("La suma de los primeros elementos  de cada Array A y B es: \(c)")

//cargar un areglo con 10 numeros al azar y determine lo siguiente: ¿cual es el numero mayor? y ¿cual es el numero menor?

var numer:[Int] = []

for i in 0..<10 {
     numer.append(Int(arc4random_uniform(98) + 1))
}
print("los numeros al azar son \(numer)")

var valorM = numer.max()
print(valorM)

var valormen = numer.min()
print(valormen)



